package uk.ac.reading.hm007240;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Map.Entry;

/**
 * @author hm007240
 * This class completes task 4 of the assignment
 */

public class Task4 {
	/**
	 * flightdata holds location of the file being read in
	 * line signifies that new lines start when an empty cell is encountered
	 * csvSplitBy signifies that new variable starts on commas
	 * flightinfo contains the information from the file being read
	 * passengerID holds the value of the passenger with the most accrued air miles
	 * mostmiles holds the value of the highest amount of air miles
	 * writetoFile is a BufferedWriter used to write the result of the program to a file
	 * reader is a BufferedReader used to read in the file data
	 * map is a map for the flightID as the key and the distance of the flight as the value
	 * airmiles is a map for the passengerID as the key and the sum of their air miles as the value
	 */
	
	public static void main(String[] args) {
		String flightdata = "/N:/AdvancedComputing/AComp_Passenger_data.csv";
		String line = "";
		String csvSplitBy = ",";
		String[] flightinfo = null;
		String passengerID = "";
		Double mostmiles = 0.0;
		BufferedWriter writetoFile;

		TreeMap<String, Double> map = new TreeMap<>();
		TreeMap<String, Double> airmiles = new TreeMap<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(flightdata))) {

			while ((line = reader.readLine()) != null) {

				// use comma as separator
				flightinfo = line.split(csvSplitBy);
				if ((flightinfo[1].matches("[A-Z]{3}[0-9]{4}[A-Z]{1}")) && (flightinfo[2].matches("[A-Z]{3}")) && (flightinfo[3].matches("[A-Z]{3}")) && (flightinfo[0].matches("[A-Z]{3}[0-9]{4}[A-Z]{2}[0-9]{1}"))){
					Double dist = distance(flightinfo[1], flightinfo[2]);
					if (map.get(flightinfo[1]) == null) {   
						map.put(flightinfo[1], dist);
					}
					if (airmiles.get(flightinfo[0]) == null) {   
						airmiles.put(flightinfo[0], dist);
					}
					else {
						double counter = airmiles.get(flightinfo[0]);
						airmiles.put(flightinfo[0], counter + dist);
					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		for (Entry<String, Double> entry : map.entrySet())
		{
			System.out.println("Flight: " + entry.getKey() + " is " + entry.getValue() + "nautical miles long");
		}
		for (Entry<String, Double> entry : airmiles.entrySet())
		{
			System.out.println("Passenger: " + entry.getKey() + " has flown " + entry.getValue() + "nautical miles!");
			if (entry.getValue() > mostmiles) {
				mostmiles = entry.getValue();
				passengerID = entry.getKey();
			}
		}
		try {
			writetoFile = new BufferedWriter(new FileWriter("N:/AdvancedComputing/airmiles.csv"));
			writetoFile.write("Passenger with most air miles is: " + passengerID + ",");
			writetoFile.write("with " + mostmiles + " miles! ,");
			writetoFile.flush();
			writetoFile.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * This class is taken from a webpage, the source of which is given in the report
	 * @param depart is the airport that the flight is departing from
	 * @param destin is the airport that the flight is heading towards
	 * filename holds location of the file being read in
	 * line signifies that new lines start when an empty cell is encountered
	 * csvSplitBy signifies that new variable starts on commas
	 * flightinfo contains the information from the file being read
	 * lat1 is the latitude value of the departing airport
	 * lat2 is the latitude value of the arrival airport
	 * long1 is the longitude value of the departing airport
	 * long2 is the longitude value of the arrival airport
	 * newmap is a map for the airport as the key and the Latitude and Longitude as the values
	 * LatandLong is a string array list that holds the latitude and longitude values of each airport
	 * R is the radius of the Earth
	 * latdistance holds the distance between the two latitudes provided
	 * longdistance holds the distance between the two longitudes provided
	 * @return distance is the distance between the two 
	 */
	private static Double distance(String depart, String destin) {
		String filename = "/N:/AdvancedComputing/Top30_airports_LatLong.csv";
		String line = "";
		String csvSplitBy = ",";
		String[] flightinfo = null;
		double lat1 = 0;
		double lat2 = 0;
		double long1 = 0;
		double long2 = 0;

		TreeMap<String, ArrayList<String>> newmap = new TreeMap<>();

		try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {

			while ((line = reader.readLine()) != null) {

				// use comma as separator
				flightinfo = line.split(csvSplitBy);
				if ((line != "") && (line !=null) && (!line.isEmpty())){
					if (flightinfo[1].matches("[A-Z]{3}")){
						if (newmap.get(flightinfo[2]) == null) {   
							ArrayList<String> LatandLong = new ArrayList<String>();
							LatandLong.add(flightinfo[2]);
							LatandLong.add(flightinfo[3]);
							newmap.put(flightinfo[1], LatandLong);
						}
					}
				}
			}



		} catch (IOException e) {
			e.printStackTrace();
		}
		for (Entry<String, ArrayList<String>> entry : newmap.entrySet())
		{
			System.out.println(entry.getKey() + "/" + entry.getValue());
			if (depart.equals(entry.getKey())) {
				lat1 = Double.valueOf(entry.getValue().get(0));
				long1 = Double.valueOf(entry.getValue().get(1));
			}
			if (destin.equals(entry.getKey())) {
				lat2 = Double.valueOf(entry.getValue().get(0));
				long2 = Double.valueOf(entry.getValue().get(1));
			}
		}
		final int R = 6371;

		double latDistance = Math.toRadians(lat2 - lat1);
		double longDistance = Math.toRadians(long2 - long1);
		double a = Math.sin(latDistance/2) * Math.sin(latDistance /2) + Math.cos(Math.toRadians(lat1))
		* Math.cos(Math.toRadians(lat2)) * Math.sin(longDistance /2) * Math.sin(longDistance /2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double distance = R * c * 1000 / 1852; //converting to miles
		distance = Math.pow(distance, 2) + Math.pow(0.0, 2);
		return Math.sqrt(distance);
	}
}
