package uk.ac.reading.hm007240;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.TimeZone;

/**
 * @author hm007240
 * This class completes task 2 of the assignment
 */
public class Task2 {

	/**
	 * flightdata holds location of the file being read in
	 * line signifies that new lines start when an empty cell is encountered
	 * csvSplitBy signifies that new variable starts on commas
	 * Writer is a BufferedWriter used to write the result of the program to a file
	 * fileinfo contains the information from the file being read
	 * reader is a BufferedReader used to read in the file data
	 * map is a map for the flightID as the key and the flight statistics as the value
	 */

	public static void main(String[] args) {
		String flightdata = "/N:/AdvancedComputing/AComp_Passenger_data.csv";
		String line = "";
		String csvSplitBy = ",";
		BufferedWriter Writer;
		String[] fileinfo = null;

		TreeMap<String, ArrayList<String>> map = new TreeMap<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(flightdata))) {

			while ((line = reader.readLine()) != null) {

				// use comma as separator
				fileinfo = line.split(csvSplitBy);
				if (fileinfo[1].matches("[A-Z]{3}[0-9]{4}[A-Z]{1}")) {
					//System.out.println("Flight leaving " + fileinfo[2] + " , and arriving in " + fileinfo[3]);
					if (map.get(fileinfo[1]) == null) {   
						ArrayList<String> flightStats = new ArrayList<String>();
						String passengerID = fileinfo[0]; // this holds the passengerID
						String airportFrom = fileinfo[2]; // this holds the airport the flight took off from
						String airportDestination = fileinfo[3]; // this holds the airport the flight arrives in
						int tempDepart = Integer.parseInt(fileinfo[4]); // this holds the depart time
						int tempLength = Integer.parseInt(fileinfo[5]) * 60 ; // this holds the flight length time 
						int tempArrive = tempDepart + tempLength; // this calculates the arrival time
						Date itemDate = new Date(tempDepart * 1000); //gets the time in milliseconds
						SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss a"); // gets the time in hours, minutes, and seconds
						sdf.setTimeZone(TimeZone.getTimeZone("GMT")); // sets time zone to GMT
						String departTime = sdf.format(itemDate);
						itemDate = new Date(tempLength * 1000);
						String flightLength = new SimpleDateFormat("hh:mm:ss").format(itemDate);
						itemDate = new Date(tempArrive * 1000);
						String arriveTime = new SimpleDateFormat("hh:mm:ss a").format(itemDate);
						/**
						 * This adds all of the info to the flightStats array list and puts it in the map with the corresponding flightID
						 */
						flightStats.add(passengerID); 
						flightStats.add(airportFrom);
						flightStats.add(airportDestination);
						flightStats.add(departTime);
						flightStats.add(flightLength);
						flightStats.add(arriveTime);
						map.put(fileinfo[1], flightStats);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			Writer = new BufferedWriter(new FileWriter("N:/AdvancedComputing/flightdata.csv"));
		for (Entry<String, ArrayList<String>> entry : map.entrySet())
		{
			System.out.println(entry.getKey() + "/" + entry.getValue());
			Writer.write("Flight: " + entry.getKey() + ",");
			Writer.write("has data: " +  entry.getValue() + ",");
		}
		Writer.flush();
		Writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
