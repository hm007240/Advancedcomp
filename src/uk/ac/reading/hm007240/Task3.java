package uk.ac.reading.hm007240;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.TreeMap;
import java.util.Map.Entry;

/**
 * @author hm007240
 * This class completes task 3 of the assignment
 */
public class Task3 {
	/**
	 * flightdata holds location of the file being read in
	 * line signifies that new lines start when an empty cell is encountered
	 * csvSplitBy signifies that new variable starts on commas
	 * counter is a counter
	 * fileWriter is a BufferedWriter used to write the result of the program to a file
	 * flights contains the information from the file being read
	 * reader is a BufferedReader used to read in the file data
	 * map is a map for the flightID and how many passengers took each flight
	 */
	public static void main(String[] args) {
		String flightdata = "/N:/AdvancedComputing/AComp_Passenger_data.csv";
		String line = "";
		String csvSplitBy = ",";
		int counter = 0;
		BufferedWriter fileWriter;
		String[] flights = null;

		TreeMap<String, Integer> map = new TreeMap<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(flightdata))) {

			while ((line = reader.readLine()) != null) {

				// use comma as separator
				flights = line.split(csvSplitBy);
				if ((flights[1].matches("[A-Z]{3}[0-9]{4}[A-Z]{1}")) && (flights[0].matches("[A-Z]{3}[0-9]{4}[A-Z]{2}[0-9]{1}")) ) {
					//System.out.println("Flight leaving " + flights[2] + " , and arriving in " + flights[3]);
					if (map.get(flights[1]) == null) {   
						map.put(flights[1], 1);
					}
					else {
						counter = map.get(flights[1]);
						counter++;
						map.replace(flights[1], counter);         		
					}
				}
			}



		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			fileWriter = new BufferedWriter(new FileWriter("N:/AdvancedComputing/passengernumbers.csv"));
			for (Entry<String, Integer> entry : map.entrySet())
			{
				System.out.println(entry.getKey() + "/" + entry.getValue());
				fileWriter.write("Flight: " + entry.getKey() + ",");
				fileWriter.write("has " + entry.getValue() + " passengers,");
			}
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
