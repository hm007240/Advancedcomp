package uk.ac.reading.hm007240;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * @author hm007240
 *This is the class for completing task 1 of the assignment
 */

public class Filefilter {
	/**
	 * flightdata holds location of the file being read in
	 * line signifies that new lines start when an empty cell is encountered
	 * csvSplitBy signifies that new variable starts on commas
	 * counter is a counter
	 * unusedcounter is a counter
	 * othercounter is a counter
	 * alldepartcount is a counter
	 * allarrivecount is a counter
	 * outputWriter is used to output the result of the program to a file
	 * found signifies if an airport has been used or not
	 * flights holds the file data
	 * unusedairports holds any unused airports
	 * uniqueflights stores list of unique flights
	 * mapofdepartures is a map holding the departing airport and the number of people who were on that flight
	 * mapofdepartanddest is a map of both the departing and arriving airport and the number of people who travelled to or from there
	 */
	public static void main(String[] args) {
		String flightdata = "/N:/AdvancedComputing/AComp_Passenger_data.csv";
		String line = "";
		String csvSplitBy = ",";
		int counter = 0;
		int unusedcount = 0;
		int othercount = 0;
		int alldepartcount = 0;
		int allarrivecount = 0;
		BufferedWriter outputWriter;
		boolean found = false;
		String[] flights = null;
		String[] unusedairports = new String[5];
		String[] uniqueflights = new String[30];

		TreeMap<String, Integer> mapofdepartures = new TreeMap<>();
		TreeMap<String, Integer> mapofdepartanddest = new TreeMap<>();

		try (BufferedReader reader = new BufferedReader(new FileReader(flightdata))) {

			while ((line = reader.readLine()) != null) {

				// use comma as separator
				flights = line.split(csvSplitBy);
				if ((flights[2].matches("[A-Z]{3}")) && (flights[3].matches("[A-Z]{3}"))) {
					//System.out.println("Flight leaving " + flights[2] + " , and arriving in " + flights[3]);
					if (mapofdepartures.get(flights[2]) == null) {   
						mapofdepartures.put(flights[2], 1);
					}
					else {
						counter = mapofdepartures.get(flights[2]);
						counter++;
						mapofdepartures.replace(flights[2], counter);         		
					}
					if (mapofdepartanddest.get(flights[2]) == null) {   
						mapofdepartanddest.put(flights[2], 1);
					}
					else {
						alldepartcount = mapofdepartanddest.get(flights[2]);
						alldepartcount++;
						mapofdepartanddest.replace(flights[2], alldepartcount);         		
					}
					if (mapofdepartanddest.get(flights[3]) == null) {   
						mapofdepartanddest.put(flights[3], 1);
					}
					else {
						allarrivecount = mapofdepartanddest.get(flights[3]);
						allarrivecount++;
						mapofdepartanddest.replace(flights[3], allarrivecount);         		
					}

				}
			}


		} catch (IOException e) {
			e.printStackTrace();
		}
		/**
		 * blah is a string that holds the returned value of the top30() method
		 * outputWriter is the BufferedWriter used to write result to a file
		 */
		String[] blah =  new String [30];
		blah = top30();
		try {
			outputWriter = new BufferedWriter(new FileWriter("N:/AdvancedComputing/numflights.csv"));
			for (Entry<String, Integer> entry : mapofdepartures.entrySet())
			{
				System.out.println(entry.getKey() + "/" + entry.getValue());
				outputWriter.write("Airport: " + entry.getKey() + ",");
				outputWriter.write("has " + entry.getValue() + " flights,");
			}
			for (Entry<String, Integer> entry : mapofdepartanddest.entrySet())
			{
				uniqueflights[othercount] = entry.getKey();
				othercount++;
			}
			for (int j = 0; j < blah.length; j++ ) {
				found = false;
				for (int i = 0; i < uniqueflights.length; i++) {
					if (blah[j].equals(uniqueflights[i])) {
						found = true;
					}
					if ((i == uniqueflights.length - 1) && (!blah[j].equals(uniqueflights[i])) && (found == false)) { 
						unusedairports[unusedcount] = blah[j]; unusedcount++; 
					}
				}
			}

			try {
				outputWriter.write("The following airports are unused: " + ",");
				for (int i = 0; i < unusedairports.length; i++) {
					if (unusedairports[i] != null) {
						outputWriter.write(unusedairports[i]+",");
					}
				}
				outputWriter.flush();
				outputWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


	} 
	/**
	 * freqdata holds location of the file being read in
	 * newline signifies that new lines start when an empty cell is encountered
	 * delimiter signifies that new variable starts on commas
	 * i is a counter
	 * j is a counter
	 * @return airportlist is a list of the top 30 most frequented airports
	 */
	public static String[] top30() {
		String freqdata = "/N:/AdvancedComputing/Top30_airports_LatLong.csv";
		String newline = "";
		String delimiter = ",";
		int i = 0;
		String[] airportlist = new String[30];
		try (BufferedReader reader2 = new BufferedReader(new FileReader(freqdata))) {

			while ((newline = reader2.readLine()) != null) {
				String[] airportdata = newline.split(delimiter);
				if ((newline != "") && (newline !=null) && (!newline.isEmpty())){
					if (airportdata[1].matches("[A-Z]{3}")){
						airportlist[i] = airportdata[1];
						i++;
					}
				}
			}
			for (int j = 0; j< 30; j++) {
				//System.out.println(airportlist[j]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return airportlist;
	}



}

